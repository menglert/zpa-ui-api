import base64
import getpass
import json

username = input("User Name: ")
password = getpass.getpass()
password_b64encode = base64.urlsafe_b64encode(password.encode("utf-8"))
password_b64encode_string = str(password_b64encode, "utf-8")

creds = {
            "username": username, 
            "password": password_b64encode_string
        }

with open("credentials.json", "w") as write_file:
    json.dump(creds, write_file)