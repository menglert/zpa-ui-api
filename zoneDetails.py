import logging
import json

logging.basicConfig(level=logging.DEBUG, filename='zpa-ui-api.log')

def getZoneDetail(conn, headers, customerId):
    conn.request("GET" , "/shift/api/v1/admin/zoneDetails?accessingCustomerId=" + customerId, headers=headers)
    res = conn.getresponse()
    data = res.read()
    zoneResponse = data.decode("utf-8")
    zoneResponseJson = json.loads(zoneResponse)
    logging.debug("Zone Details Response: " + zoneResponse)

    druid = zoneResponseJson['serviceEndpoints']['service.zpa.druid']
    drilldowns = zoneResponseJson['serviceEndpoints']['service.zpa.drilldowns']
    logging.debug("Druid Endpoint: " + druid)
    logging.debug("Drilldowns Endpoint: " + drilldowns)

    return drilldowns