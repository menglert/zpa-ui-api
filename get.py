import http.client
import json
import sys
import argparse
from login import headers
from login import customerId
from login import host
from config import page_size

def doGetPage(page, target):
    conn = http.client.HTTPSConnection(host[0])
    conn.request("GET", "/zpn/api/v1/admin/customers/" + customerId + "/" + target + "?page=" + str(page) + "&pagesize=" + str(page_size), 
            headers=headers, 
            )
    respJson = json.loads(conn.getresponse().read().decode("utf-8"))

    print(json.dumps(respJson['list'],indent=4))
    conn.close()
    
    if int(respJson['totalPages']) > page:
        page += 1
        doGetPage(page, target)

def doGet(target):
    conn = http.client.HTTPSConnection(host[0])
    conn.request("GET", "/zpn/api/v1/admin/customers/" + customerId + "/" + target, 
            headers=headers, 
            )
    respJson = json.loads(conn.getresponse().read().decode("utf-8"))

    print(json.dumps(respJson,indent=4))
    conn.close()

#session_cookies, customer_id, headers = doLogin()
arg_parser = argparse.ArgumentParser(description='Get Configuration Items from the ZPA Admin Portal')
pageList = ['application', 'clientless', 'applicationGroup', 'server', 'serverGroup', 'clientlessCertificate', 'assistant', 'assistantGroup', 'audit', 'userPortal', 'userPortalLink', 'userPortalZappLink']
noPageList = ['idp', 'samlAttribute', 'signingCert', 'siemConfig', 'policySet/global/summary', 'policySet/reauth', 'policySet/bypass', 'userportal/aup', 'roles', 'v2/associationtype/SEARCH_SUFFIX/domains']
arg_parser.add_argument(
                            '--target',
                            type=str,
                            required=True,
                            help='The Target of Configuration Item to Get',
                            choices=pageList + noPageList
                        )

args = arg_parser.parse_args()

if args.target in noPageList:
    doGet(args.target)
else:
    #print("[")
    doGetPage(1,args.target)
    #print("]")