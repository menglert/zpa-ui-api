import http.client
import requests
import logging
import json
from login import headers
from login import customerId
from login import host

logging.basicConfig(level=logging.DEBUG, filename='zpa-ui-api.log')

def doGetCsv(target, single):
    conn = http.client.HTTPSConnection(host[0])
    conn.request("GET", "/zpn/api/v1/admin/customers/" + customerId + "/v2/" + target + "/export?single=" + single, 
            headers=headers, 
            )
    respCsv = conn.getresponse().read().decode("utf-8")

    csv = open(f"{target}.csv", "w")
    csv.write(respCsv)
    csv.close()

def doPostIpBindingsCsv():
    payload ='{"timeZoneId":"CET","timeZoneStr":"Europe/Berlin","filterDtoList":[],"sortBy":{"sortName":"ip","sortOrder":"ASC"}}'
    headers['Accept'] = "*/*"
    del headers['cookie']
    logging.debug(headers)

    resp = requests.post(f"https://iparsread.private.zscaler.com/ipars/v1/customers/{customerId}/ipBindings/export",data=payload,headers=headers)

    logging.debug(resp.text)

    csv = open("ipbindings.csv", "w")
    csv.write(resp.text)
    csv.close()

#doGetCsv("application", "false")
doPostIpBindingsCsv()