import http.client
import json
from login import headers
from login import customerId
from login import host
from config import page_size

def doGetPage(page):
    conn = http.client.HTTPSConnection(host[0])
    conn.request("GET", "/zpn/api/v1/admin/customers/" + customerId + "/assistant?page=" + str(page) + "&pagesize=" + str(page_size), 
            headers=headers, 
            )
    respJson = json.loads(conn.getresponse().read().decode("utf-8"))

    print(json.dumps(respJson,indent=4))
    conn.close()
    
    if int(respJson['totalPages']) > page:
        page += 1
        doGetPage(page)

doGetPage(1)