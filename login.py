import http.client
import json
import getpass
import keyring
import logging
import urllib
import re

logging.basicConfig(level=logging.DEBUG, filename='zpa-ui-api.log')

try:
    configFile = open("config.json")
    logging.info("Reading configuration from: " + configFile.name)

    configJson = json.load(configFile)
    adminUser = configJson["adminUser"]
    zpaCloud = configJson["zpaCloud"]
    adminPassword = keyring.get_password("zpa-user", adminUser)
    if adminPassword is None:
        raise IOError("No Credential set.")
    logging.info("Read Password for Admin User: " + adminUser + " for Cloud: " + zpaCloud)
except IOError:
    logging.info("No config present. Creating one.")
    configJson = {}
    adminUser = input("Admin User: ")
    adminPassword = getpass.getpass(prompt="Admin Password: ")
    zpaCloud = input("Zscaler Cloud: ")
    configJson["adminUser"] = adminUser
    configJson["zpaCloud"] = zpaCloud
    configFile = open("config.json", "w")
    json.dump(configJson, configFile)
    logging.info("Set Password for Admin User: " + adminUser + " for Cloud: " + zpaCloud + " in " + configFile.name)

    configFile.close()
    keyring.set_password("zpa-user", adminUser, adminPassword)
    logging.info("Wrote Password to Keyring")
finally:
    configFile.close()

conn = http.client.HTTPSConnection(zpaCloud)
conn.request("GET", "/js/config.js")
configJs = conn.getresponse().read().decode("utf-8")
authnHost = re.findall(r'^\s*authnHost\s*:\s*\'https://(.*)/\',$', configJs, re.MULTILINE)
host = re.findall(r'^\s*host\s*:\s*\'https://(.*)\'\+\'.*\',$', configJs, re.MULTILINE)
conn.close()

payload = {
    'username': adminUser,
    'password': adminPassword
    }
headers = {
    'content-type': 'application/x-www-form-urlencoded'
}
logging.debug("Using Headers: " + str(headers))

conn = http.client.HTTPSConnection(authnHost[0])
conn.request("POST", "/authn/v1/oauth/token?grant_type=USER", body=urllib.parse.urlencode(payload), headers=headers)
res = conn.getresponse()
data = res.read()
loginResponse = data.decode("utf-8")
logging.debug("Auth Response: " + loginResponse)

loginResponseJson = json.loads(loginResponse)
customerId = loginResponseJson['customerId']
logging.debug("Customer ID: " + customerId)

bearerToken = loginResponseJson['Z-AUTH-TOKEN']
cookie = ';'.join(res.headers.get_all("Set-Cookie"))
logging.debug("Received Cookie: " + cookie)

headers =   {
    'Authorization': 'Bearer ' + bearerToken,
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'cookie': cookie
    }
logging.debug("Using Headers: " + str(headers))
logging.info("Logged In.")
conn.close()