import http.client
import json
import sys
import argparse
from zoneDetails import getZoneDetail
from login import headers
from login import customerId
from login import conn

null = None
pageSize = "20"

def doGetPage(page, target, startTime, endTime):
    data = json.dumps({
        "logType": "txn",
        "pageNumber": str(page),
        "pageSize": pageSize,
        "startTime": int(startTime),
        "endTime": int(endTime),
        "sortBy": null,
        "filterBy":[{"filterName":"txnStatsType","operator":"EQ","value":"errorLogs"}]
        })

    response_json = json.loads(
        conn.request.post(
            drilldowns + 
            "/zpadrilldownservice/zpn/diagnostics/v3/customers/" + 
            customerId + 
            "/latestUserTxn/startTime/" + 
            startTime + 
            "/endTime/" + 
            endTime + 
            "/pageSize/" + 
            pageSize, 
            headers=headers, 
            data=data).text
    )
    print(json.dumps(response_json['list'],indent=4))
    
    if int(response_json['totalLogs'])/page > pageSize:
        page += 1
        doGetPage(page, target, startTime, endTime)

#session_cookies, customer_id, headers = doLogin()
drilldowns = getZoneDetail(conn, headers, customerId)
arg_parser = argparse.ArgumentParser(description='Get Diagnostic Logs from the ZPA Admin Portal')
pageList = ['latestUserTxn']
arg_parser.add_argument(
                            '--target',
                            type=str,
                            required=True,
                            help='The Target of Diagnostic Logs to Get',
                            choices=pageList
                        )
arg_parser.add_argument(
                            '--startTime',
                            type=str,
                            required=True,
                            help='The Start Time of Diagnostic Logs to Get in Epoch Time',
                        )
arg_parser.add_argument(
                            '--endTime',
                            type=str,
                            required=True,
                            help='The Start Time of Diagnostic Logs to Get in Epoch Time',
                        )

args = arg_parser.parse_args()

doGetPage(1, args.target, args.startTime, args.endTime)