import json
import sys
import argparse
from datetime import datetime

with open('results.json', 'r') as json_file:
    results = json.load(json_file)
    for result in results:
        epoch_seconds = int(result['origConnStartTime']) / 1000000.0
        print(datetime.fromtimestamp(epoch_seconds).strftime("%Y/%m/%d %H:%M")+ "," + result['connectionId'] + "," + result['status'] + "," + result['domain'] + "," + result['connectorName'] + "," + result['brokerName'] + "," + result['userName'])